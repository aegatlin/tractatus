export class PreClaim {
  constructor(public location: string, public text: string, public footnote?: string) { }
}